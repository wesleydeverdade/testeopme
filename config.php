<?php
ini_set('default_charset', 'UTF-8');
ini_set('mbstring.http_output', 'UTF-8');
ini_set('mbstring.internal_encoding', 'UTF-8'); 
// PARA MANTER AS SESSÕES POR 2H
ini_set('session.gc_maxlifetime', 7200);
session_set_cookie_params(7200);
session_cache_expire(120);
session_cache_limiter('private');

if(!isset($_SESSION)) @session_start();

// define o time zone
if (function_exists('date_default_timezone_set')) date_default_timezone_set('America/Sao_Paulo');

// variáveis globais do sistema
define('DIR_SEPARATOR', '\\');
define('URL_SEPARATOR', '/');

define('APP_FOLDER', 'app');
define('TMP_FOLDER', '__tmp');
define('DOWNLOAD_FOLDER', '__download');

define('PAR_VERSION',  		 '1.0');
define('PAR_ROOT_DIR' , 	 __DIR__ . DIR_SEPARATOR );
define('PAR_URL', 			 $_SERVER["SERVER_NAME"] . URL_SEPARATOR );
define('PAR_UPLOAD_DIR',	 PAR_ROOT_DIR . DOWNLOAD_FOLDER . DIR_SEPARATOR );
define('PAR_UPLOAD_URL',	 PAR_URL. DOWNLOAD_FOLDER . URL_SEPARATOR);
define('PAR_TMP_DIR',		 PAR_ROOT_DIR . TMP_FOLDER . DIR_SEPARATOR);
define('PAR_TMP_URL',		 PAR_URL . TMP_FOLDER . URL_SEPARATOR);
define('PAR_HOME',			 PAR_URL . APP_FOLDER . URL_SEPARATOR); 