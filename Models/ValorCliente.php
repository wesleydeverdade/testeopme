<?php 

require_once "../config.php";
require_once "../_services/Connection.php";

class Valor_cliente { 

    protected $con;
    protected $oCon;

    private $id;
    private $fk_id_cliente;
    private $fk_tipo_valor;
    private $valor;


    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    function __construct(){
        $this->con  = new Connection();
        $this->oCon = $this->con->openConnection();
    }

    function __destruct() {
        unset($this->oCon); unset($this->con);
    }
    
    public function find($id){
        $stmt = $this->oCon->prepare('
            SELECT "Valor_cliente", valor_cliente.* 
            FROM valor_cliente 
            WHERE id = :id
            ');
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        
        return $stmt->fetchObject(__CLASS__);      
    }

    public function findAll(){
        $stmt = $this->oCon->prepare('SELECT * FROM valor_cliente ');
        $stmt->execute();
        return $stmt->fetchAll();  
    }
    

    public function save(){
        if (!is_null($this->id)) { return $this->update();  }

        $stmt = $this->oCon->prepare('
            INSERT INTO  valor_cliente
            (id,fk_id_cliente,fk_tipo_valor,valor) 
            VALUES 
            (:id,:fk_id_cliente,:fk_tipo_valor,:valor)'); 

        $stmt->bindParam(':id', $this->id);  
        $stmt->bindParam(':fk_id_cliente', $this->fk_id_cliente);  
        $stmt->bindParam(':fk_tipo_valor', $this->fk_tipo_valor);  
        $stmt->bindParam(':valor', $this->valor);  

        return $stmt->execute();
    }

    public function update(){
        if (!isset($this->id) || (int)$this->id == 0 ) {
            throw new \LogicException(
                'Cannot update valorCliente that does not yet exist in the database.'
            );
        }

        $stmt = $this->oCon->prepare('
            UPDATE valor_cliente SET
            fk_id_cliente = :fk_id_cliente ,  
            fk_tipo_valor = :fk_tipo_valor ,  
            valor = :valor WHERE id = :id
            ');
        
        $stmt->bindParam(':id', $this->id);  
        $stmt->bindParam(':fk_id_cliente', $this->fk_id_cliente);  
        $stmt->bindParam(':fk_tipo_valor', $this->fk_tipo_valor);  
        $stmt->bindParam(':valor', $this->valor);  

        return $stmt->execute();
    }

     public function delete(){
        if (!isset($this->id) || (int)$this->id == 0 ) {
            throw new \LogicException(
                'Cannot delete valor_cliente that does not yet exist in the database.'
            );
        }

        $stmt = $this->oCon->prepare('DELETE FROM valor_cliente WHERE id = :id');
        $stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
        return $stmt->execute();
    }
} 