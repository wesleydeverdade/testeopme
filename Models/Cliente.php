<?php 

require_once '../../config.php';
require_once "../../_services/Connection.php";

class Cliente { 

    protected $con;
    protected $oCon;

     private $id;
         private $nome;
         private $email;
         private $logo;
         private $dt_inclusao;
         private $dt_alteracao;
         private $complemento_endereco;
         private $data_expiracao;
         private $numero;
         private $status;
         private $pais_id;
         private $estado_id;
         private $regiao_id;
         private $logradouro_id;
         private $cidade_id;
         private $bairro_id;
        

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    function __construct(){
        $this->con  = new Connection();
        $this->oCon = $this->con->openConnection();
    }

    function __destruct() {
        unset($this->oCon); unset($this->con);
    }
    
    public function find($id){
        $stmt = $this->oCon->prepare('
        SELECT "Cliente", cliente.* 
        FROM cliente 
        WHERE id = :id
        ');
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        
        return $stmt->fetchObject(__CLASS__);      
    }

    public function findAll(){
        $stmt = $this->oCon->prepare('SELECT * FROM cliente ');
        $stmt->execute();
        return $stmt->fetchAll();  
    }
    

    public function save(){
        if (!is_null($this->id)) { return $this->update();  }

        $stmt = $this->oCon->prepare('
        INSERT INTO  cliente
        (id,nome,email,logo,dt_inclusao,dt_alteracao,complemento_endereco,data_expiracao,numero,status,pais_id,estado_id,regiao_id,logradouro_id,cidade_id,bairro_id) 
        VALUES 
        (:id,:nome,:email,:logo,:dt_inclusao,:dt_alteracao,:complemento_endereco,:data_expiracao,:numero,:status,:pais_id,:estado_id,:regiao_id,:logradouro_id,:cidade_id,:bairro_id)'); 

         $stmt->bindParam(':id', $this->id);  
             $stmt->bindParam(':nome', $this->nome);  
             $stmt->bindParam(':email', $this->email);  
             $stmt->bindParam(':logo', $this->logo);  
             $stmt->bindParam(':dt_inclusao', $this->dt_inclusao);  
             $stmt->bindParam(':dt_alteracao', $this->dt_alteracao);  
             $stmt->bindParam(':complemento_endereco', $this->complemento_endereco);  
             $stmt->bindParam(':data_expiracao', $this->data_expiracao);  
             $stmt->bindParam(':numero', $this->numero);  
             $stmt->bindParam(':status', $this->status);  
             $stmt->bindParam(':pais_id', $this->pais_id);  
             $stmt->bindParam(':estado_id', $this->estado_id);  
             $stmt->bindParam(':regiao_id', $this->regiao_id);  
             $stmt->bindParam(':logradouro_id', $this->logradouro_id);  
             $stmt->bindParam(':cidade_id', $this->cidade_id);  
             $stmt->bindParam(':bairro_id', $this->bairro_id);  
            
        return $stmt->execute();
    }

    public function update(){
        if (!isset($this->id) || (int)$this->id == 0 ) {
            throw new \LogicException(
            'Cannot update cliente that does not yet exist in the database.'
            );
        }

        $stmt = $this->oCon->prepare('
        UPDATE cliente SET
                 nome = :nome ,  
                 email = :email ,  
                 logo = :logo ,  
                 dt_inclusao = :dt_inclusao ,  
                 dt_alteracao = :dt_alteracao ,  
                 complemento_endereco = :complemento_endereco ,  
                 data_expiracao = :data_expiracao ,  
                 numero = :numero ,  
                 status = :status ,  
                 pais_id = :pais_id ,  
                 estado_id = :estado_id ,  
                 regiao_id = :regiao_id ,  
                 logradouro_id = :logradouro_id ,  
                 cidade_id = :cidade_id ,  
                 bairro_id = :bairro_id WHERE id = :id
        ');
        
         $stmt->bindParam(':id', $this->id);  
             $stmt->bindParam(':nome', $this->nome);  
             $stmt->bindParam(':email', $this->email);  
             $stmt->bindParam(':logo', $this->logo);  
             $stmt->bindParam(':dt_inclusao', $this->dt_inclusao);  
             $stmt->bindParam(':dt_alteracao', $this->dt_alteracao);  
             $stmt->bindParam(':complemento_endereco', $this->complemento_endereco);  
             $stmt->bindParam(':data_expiracao', $this->data_expiracao);  
             $stmt->bindParam(':numero', $this->numero);  
             $stmt->bindParam(':status', $this->status);  
             $stmt->bindParam(':pais_id', $this->pais_id);  
             $stmt->bindParam(':estado_id', $this->estado_id);  
             $stmt->bindParam(':regiao_id', $this->regiao_id);  
             $stmt->bindParam(':logradouro_id', $this->logradouro_id);  
             $stmt->bindParam(':cidade_id', $this->cidade_id);  
             $stmt->bindParam(':bairro_id', $this->bairro_id);  
             
        return $stmt->execute();
    }

     public function delete(){
        if (!isset($this->id) || (int)$this->id == 0 ) {
            throw new \LogicException(
                'Cannot delete cidade that does not yet exist in the database.'
            );
        }

        $stmt = $this->oCon->prepare('DELETE FROM cidade WHERE id = :id');
        $stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
        return $stmt->execute();
    }
} 