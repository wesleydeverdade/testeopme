<?php 

require_once '../config.php';
require_once '../_services/Connection.php';

class User { 

    protected $con;
    protected $oCon; 

    private $id;
    private $username;
    private $firstname;
    private $lastname;
    private $email;
    private $password;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    function __construct(){
        $this->con  = new Connection();
        $this->oCon = $this->con->openConnection();
    }

    function __destruct() {
        unset($this->oCon); unset($this->con);
    }

    public function find($id){
        $stmt = $this->oCon->prepare('
            SELECT "User", user.* 
            FROM user 
            WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        
        return $stmt->fetch();      
    }

    public function findAll(){
        $ret = $this->oCon->query('SELECT * FROM user'); 
        return $ret->fetchAll();
    }

    public function save(){
        if (!is_null($this->id)) { return $this->update();  }

        $stmt = $this->oCon->prepare('
            INSERT INTO user
                (username, firstname, lastname, email,password) 
            VALUES 
                (:username, :firstname , :lastname, :email, :password)');

        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':firstname', $this->firstname);
        $stmt->bindParam(':lastname', $this->lastname);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':password', $this->password);
        return $stmt->execute();
    }

    public function update(){
        if (!isset($this->id) || (int)$this->id == 0 ) {
            throw new \LogicException(
                'Cannot update user that does not yet exist in the database.'
            );
        }

        $stmt = $this->oCon->prepare('
            UPDATE user
            SET username = :username,
                firstname = :firstname,
                lastname = :lastname,
                email = :email,
                password = :password
            WHERE id = :id
        ');
        
        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':firstname', $this->firstname);
        $stmt->bindParam(':lastname', $this->lastname);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
        return $stmt->execute();
    }

    public function delete(){
        if (!isset($this->id) || (int)$this->id == 0 ) {
            throw new \LogicException(
                'Cannot delete user that does not yet exist in the database.'
            );
        }

        $stmt = $this->oCon->prepare('DELETE FROM user WHERE id = :id');
        $stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
        return $stmt->execute();
    }
}