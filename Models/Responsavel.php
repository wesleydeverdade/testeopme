<?php 

require_once "../config.php";
require_once "../_services/Connection.php";

class Responsavel { 

    protected $con;
    protected $oCon;

    private $id;
    private $nome;
    private $telefone;
    private $celular;
    private $email;


    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    function __construct(){
        $this->con  = new Connection();
        $this->oCon = $this->con->openConnection();
    }

    function __destruct() {
        unset($this->oCon); unset($this->con);
    }
    
    public function find($id){
        $stmt = $this->oCon->prepare('
            SELECT "Responsavel", responsavel.* 
            FROM responsavel 
            WHERE id = :id
            ');
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        
        return $stmt->fetchObject(__CLASS__);      
    }

    public function findAll(){
        $stmt = $this->oCon->prepare('SELECT * FROM responsavel ');
        $stmt->execute();
        return $stmt->fetchAll();  
    }
    

    public function save(){
        if (!is_null($this->id)) { return $this->update();  }

        $stmt = $this->oCon->prepare('
            INSERT INTO  responsavel
            (id,nome,telefone,celular,email) 
            VALUES 
            (:id,:nome,:telefone,:celular,:email)'); 

        $stmt->bindParam(':id', $this->id);  
        $stmt->bindParam(':nome', $this->nome);  
        $stmt->bindParam(':telefone', $this->telefone);  
        $stmt->bindParam(':celular', $this->celular);  
        $stmt->bindParam(':email', $this->email);  

        return $stmt->execute();
    }

    public function update(){
        if (!isset($this->id) || (int)$this->id == 0 ) {
            throw new \LogicException(
                'Cannot update responsavel that does not yet exist in the database.'
            );
        }

        $stmt = $this->oCon->prepare('
            UPDATE responsavel SET
            nome = :nome ,  
            telefone = :telefone ,  
            celular = :celular ,  
            email = :email WHERE id = :id
            ');
        
        $stmt->bindParam(':id', $this->id);  
        $stmt->bindParam(':nome', $this->nome);  
        $stmt->bindParam(':telefone', $this->telefone);  
        $stmt->bindParam(':celular', $this->celular);  
        $stmt->bindParam(':email', $this->email);  

        return $stmt->execute();
    }

     public function delete(){
        if (!isset($this->id) || (int)$this->id == 0 ) {
            throw new \LogicException(
                'Cannot delete responsavel that does not yet exist in the database.'
            );
        }

        $stmt = $this->oCon->prepare('DELETE FROM responsavel WHERE id = :id');
        $stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
        return $stmt->execute();
    }
} 