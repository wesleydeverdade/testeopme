<?php 

require_once "../config.php";
require_once "../_services/Connection.php";

class Cliente_responsavel { 

    protected $con;
    protected $oCon;

    private $id;
    private $fk_cliente_id;
    private $fk_responsavel_id;
    

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    function __construct(){
        $this->con  = new Connection();
        $this->oCon = $this->con->openConnection();
    }

    function __destruct() {
        unset($this->oCon); unset($this->con);
    }
    
    public function find($id){
        $stmt = $this->oCon->prepare('
            SELECT "Cliente_responsavel", cliente_responsavel.* 
            FROM cliente_responsavel 
            WHERE id = :id
            ');
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        
        return $stmt->fetchObject(__CLASS__);      
    }

    public function findAll(){
        $stmt = $this->oCon->prepare('SELECT * FROM cliente_responsavel ');
        $stmt->execute();
        return $stmt->fetchAll();  
    }
    

    public function save(){
        if (!is_null($this->id)) { return $this->update();  }

        $stmt = $this->oCon->prepare('
            INSERT INTO  cliente_responsavel
            (id,fk_cliente_id,fk_responsavel_id) 
            VALUES 
            (:id,:fk_cliente_id,:fk_responsavel_id)'); 

        $stmt->bindParam(':id', $this->id);  
        $stmt->bindParam(':fk_cliente_id', $this->fk_cliente_id);  
        $stmt->bindParam(':fk_responsavel_id', $this->fk_responsavel_id);  
        
        return $stmt->execute();
    }

    public function update(){
        if (!isset($this->id) || (int)$this->id == 0 ) {
            throw new \LogicException(
                'Cannot update cliente_responsavel that does not yet exist in the database.'
            );
        }

        $stmt = $this->oCon->prepare('
            UPDATE cliente_responsavel SET
            fk_cliente_id = :fk_cliente_id ,  
            fk_responsavel_id = :fk_responsavel_id WHERE id = :id
            ');
        
        $stmt->bindParam(':id', $this->id);  
        $stmt->bindParam(':fk_cliente_id', $this->fk_cliente_id);  
        $stmt->bindParam(':fk_responsavel_id', $this->fk_responsavel_id);  
        
        return $stmt->execute();
    }

    public function delete(){
        if (!isset($this->id) || (int)$this->id == 0 ) {
            throw new \LogicException(
                'Cannot delete cliente_responsavel that does not yet exist in the database.'
            );
        }

        $stmt = $this->oCon->prepare('DELETE FROM cliente_responsavel WHERE id = :id');
        $stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
        return $stmt->execute();
    }
} 