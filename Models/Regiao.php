<?php 

require_once "../config.php";
require_once "../_services/Connection.php";

class Regiao { 

    protected $con;
    protected $oCon;

    private $id;
    private $nome;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    function __construct(){
        $this->con  = new Connection();
        $this->oCon = $this->con->openConnection();
    }

    function __destruct() {
        unset($this->oCon); unset($this->con);
    }
    
    public function find($id){
        $stmt = $this->oCon->prepare('
            SELECT "Regiao", regiao.* 
            FROM regiao 
            WHERE id = :id
            ');
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        
        return $stmt->fetchObject(__CLASS__);      
    }

    public function findAll(){
        $stmt = $this->oCon->prepare('SELECT * FROM regiao ');
        $stmt->execute();
        return $stmt->fetchAll();  
    }
    

    public function save(){
        if (!is_null($this->id)) { return $this->update();  }

        $stmt = $this->oCon->prepare('
            INSERT INTO  regiao
            (id,nome) 
            VALUES 
            (:id,:nome)'); 

        $stmt->bindParam(':id', $this->id);  
        $stmt->bindParam(':nome', $this->nome);  

        return $stmt->execute();
    }

    public function update(){
        if (!isset($this->id) || (int)$this->id == 0 ) {
            throw new \LogicException(
                'Cannot update regiao that does not yet exist in the database.'
            );
        }

        $stmt = $this->oCon->prepare('
            UPDATE regiao SET
            nome = :nome WHERE id = :id
            ');
        
        $stmt->bindParam(':id', $this->id);  
        $stmt->bindParam(':nome', $this->nome);  

        return $stmt->execute();
    }

     public function delete(){
        if (!isset($this->id) || (int)$this->id == 0 ) {
            throw new \LogicException(
                'Cannot delete regiao that does not yet exist in the database.'
            );
        }

        $stmt = $this->oCon->prepare('DELETE FROM regiao WHERE id = :id');
        $stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
        return $stmt->execute();
    }
} 