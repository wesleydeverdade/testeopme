<?php 
 
require_once '../../config.php';
require_once '../../_services/Connection.php';

class ListarClientes { 

    protected $con;
    protected $oCon; 

    private $nome;
    private $cnpj;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    function __construct(){
        $this->con  = new Connection();
        $this->oCon = $this->con->openConnection();
    }

    function __destruct() {
        unset($this->oCon); unset($this->con);
    }

    public function listaComFiltro(){
        $stmt = $this->oCon->prepare('
            SELECT  *
            FROM cliente 
            WHERE nome LIKE :nome or cnpj LIKE :cnpj
        ');
        $stmt->bindValue(':nome', '%'.$this->nome.'%');
        $stmt->bindValue(':cnpj', '%'.$this->cnpj.'%');
        $stmt->execute();
        
        return $stmt->fetchAll();      
    }
 
}