<?php 
 
require_once '../../config.php';
require_once '../../_services/Connection.php';
require_once '../../Models/Cliente.php';

class VisualizarCliente { 

    protected $con;
    protected $oCon; 

    private $id_cliente;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    function __construct(){
        $this->con  = new Connection();
        $this->oCon = $this->con->openConnection();
    }

    function __destruct() {
        unset($this->oCon); unset($this->con);
    }

    public function listaPorId(){
        $mCli = new Cliente();
        return $mCli->find($this->id_cliente);
    }
 
}