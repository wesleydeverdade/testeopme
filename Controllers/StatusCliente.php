<?php 
 
require_once '../../config.php';
require_once '../../_services/Connection.php';

class StatusCliente { 

    protected $con;
    protected $oCon; 

    private $id_cliente;
    private $status;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    function __construct(){
        $this->con  = new Connection();
        $this->oCon = $this->con->openConnection();
    }

    function __destruct() {
        unset($this->oCon); unset($this->con);
    }

    public function atualizarCliente(){
         $stmt = $this->oCon->prepare('
            UPDATE cliente SET
            status = :status WHERE id = :id
            ');
        
        $stmt->bindParam(':id', $this->id_cliente);  
        $stmt->bindParam(':status', $this->status);  
        
        return $stmt->execute();      
    }
 
}