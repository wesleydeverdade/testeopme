-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 07-Nov-2019 às 22:43
-- Versão do servidor: 10.4.8-MariaDB
-- versão do PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `gestao_opme`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `bairro`
--

CREATE TABLE `bairro` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `bairro`
--

INSERT INTO `bairro` (`id`, `nome`) VALUES
(1, 'Vila Nova'),
(2, 'Campo Novo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cidade`
--

CREATE TABLE `cidade` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `cidade`
--

INSERT INTO `cidade` (`id`, `nome`) VALUES
(1, 'Porto Alegre'),
(2, 'Florianópolis');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `logo` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `dt_inclusao` datetime DEFAULT NULL,
  `dt_alteracao` datetime DEFAULT NULL,
  `complemento_endereco` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `data_expiracao` datetime DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `pais_id` int(11) NOT NULL,
  `estado_id` int(11) NOT NULL,
  `regiao_id` int(11) NOT NULL,
  `logradouro_id` int(11) NOT NULL,
  `cidade_id` int(11) NOT NULL,
  `bairro_id` int(11) NOT NULL,
  `cnpj` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `email`, `logo`, `dt_inclusao`, `dt_alteracao`, `complemento_endereco`, `data_expiracao`, `numero`, `status`, `pais_id`, `estado_id`, `regiao_id`, `logradouro_id`, `cidade_id`, `bairro_id`, `cnpj`) VALUES
(1, '1', 'OFAKSDOFKADO', 'SDOKFOSKDFO', '2019-11-12 00:00:00', '2019-11-12 00:00:00', 'fudfahdfui', '2019-11-12 00:00:00', 4132, 1, 1, 1, 2, 2, 2, 2, '1234');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente_responsavel`
--

CREATE TABLE `cliente_responsavel` (
  `id` int(1) NOT NULL,
  `fk_cliente_id` int(11) NOT NULL,
  `fk_responsavel_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `estado`
--

CREATE TABLE `estado` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `estado`
--

INSERT INTO `estado` (`id`, `nome`) VALUES
(1, 'Rio Grande do Sul'),
(2, 'Santa Catarina');

-- --------------------------------------------------------

--
-- Estrutura da tabela `logradouro`
--

CREATE TABLE `logradouro` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `logradouro`
--

INSERT INTO `logradouro` (`id`, `nome`) VALUES
(1, 'Estrada Cristiano Kraemer'),
(2, 'Av. Independência');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pais`
--

CREATE TABLE `pais` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `pais`
--

INSERT INTO `pais` (`id`, `nome`) VALUES
(1, 'Brasil'),
(2, 'Índia');

-- --------------------------------------------------------

--
-- Estrutura da tabela `regiao`
--

CREATE TABLE `regiao` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `regiao`
--

INSERT INTO `regiao` (`id`, `nome`) VALUES
(1, 'Sul'),
(2, 'Norte');

-- --------------------------------------------------------

--
-- Estrutura da tabela `responsavel`
--

CREATE TABLE `responsavel` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `telefone` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `celular` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_valor`
--

CREATE TABLE `tipo_valor` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Users';

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `username`, `firstname`, `lastname`, `email`, `password`) VALUES
(1, 'wesleydeverdade', 'WWWW', 'Magalhães', 'wesleymagalhaes5@hotmail.com', ''),
(2, 'wesleydeverdade', 'Wesley', 'Magalhães', 'wesleymagalhaes5@hotmail.com', ''),
(3, 'wesleydeverdade', 'Wesley', 'Magalhães', 'wesleymagalhaes5@hotmail.com', ''),
(4, 'wesleydeverdade', 'Wesley', 'Magalhães', 'wesleymagalhaes5@hotmail.com', ''),
(5, 'wesleydeverdade', 'Wesley', 'Magalhães', 'wesleymagalhaes5@hotmail.com', ''),
(6, 'wesleydeverdade', 'Wesley', 'Magalhães', 'wesleymagalhaes5@hotmail.com', ''),
(7, 'wesleydeverdade', 'Wesley', 'Magalhães', 'wesleymagalhaes5@hotmail.com', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `valor_cliente`
--

CREATE TABLE `valor_cliente` (
  `id` int(11) NOT NULL,
  `fk_id_cliente` int(11) NOT NULL,
  `fk_tipo_valor` int(11) NOT NULL,
  `valor` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `bairro`
--
ALTER TABLE `bairro`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `cidade`
--
ALTER TABLE `cidade`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pais_id` (`pais_id`),
  ADD KEY `estado_id` (`estado_id`),
  ADD KEY `regiao_id` (`regiao_id`),
  ADD KEY `logradouro_id` (`logradouro_id`),
  ADD KEY `cidade_id` (`cidade_id`),
  ADD KEY `bairro_id` (`bairro_id`);

--
-- Índices para tabela `cliente_responsavel`
--
ALTER TABLE `cliente_responsavel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cliente_has_responsavel_cliente` (`fk_cliente_id`),
  ADD KEY `fk_cliente_has_responsavel_responsavel1` (`fk_responsavel_id`);

--
-- Índices para tabela `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `logradouro`
--
ALTER TABLE `logradouro`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `regiao`
--
ALTER TABLE `regiao`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `responsavel`
--
ALTER TABLE `responsavel`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `tipo_valor`
--
ALTER TABLE `tipo_valor`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index2` (`username`);

--
-- Índices para tabela `valor_cliente`
--
ALTER TABLE `valor_cliente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_valor_cliente_cliente1` (`fk_id_cliente`),
  ADD KEY `fk_valor_cliente_tipo_valor1` (`fk_tipo_valor`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `bairro`
--
ALTER TABLE `bairro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `cidade`
--
ALTER TABLE `cidade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `estado`
--
ALTER TABLE `estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `logradouro`
--
ALTER TABLE `logradouro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `pais`
--
ALTER TABLE `pais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `regiao`
--
ALTER TABLE `regiao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `responsavel`
--
ALTER TABLE `responsavel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `valor_cliente`
--
ALTER TABLE `valor_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `bairro_id` FOREIGN KEY (`bairro_id`) REFERENCES `bairro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cidade_id` FOREIGN KEY (`cidade_id`) REFERENCES `cidade` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `estado_id` FOREIGN KEY (`estado_id`) REFERENCES `estado` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `logradouro_id` FOREIGN KEY (`logradouro_id`) REFERENCES `logradouro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pais_id` FOREIGN KEY (`pais_id`) REFERENCES `pais` (`id`),
  ADD CONSTRAINT `regiao_id` FOREIGN KEY (`regiao_id`) REFERENCES `regiao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `cliente_responsavel`
--
ALTER TABLE `cliente_responsavel`
  ADD CONSTRAINT `fk_cliente_has_responsavel_cliente` FOREIGN KEY (`fk_cliente_id`) REFERENCES `cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cliente_has_responsavel_responsavel1` FOREIGN KEY (`fk_responsavel_id`) REFERENCES `responsavel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `valor_cliente`
--
ALTER TABLE `valor_cliente`
  ADD CONSTRAINT `fk_valor_cliente_cliente1` FOREIGN KEY (`fk_id_cliente`) REFERENCES `cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_valor_cliente_tipo_valor1` FOREIGN KEY (`fk_tipo_valor`) REFERENCES `tipo_valor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
