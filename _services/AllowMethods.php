<?php

class AllowMethods{

	private $method;
	
	public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

	function __construct(){
 		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');
    }

    function __destruct() {
 
    }
	
	function start(){
		switch ($this->method) {
			case "GET":    $this->methodGet(); break;
			case "POST":   $this->methodPost(); break;
			case "PUT":    $this->methodPut(); break;
			case "DELETE": $this->methodDelete(); break;
			default: 	   $this->methodGet(); break;
		}
	}

	public function methodGet(){ header('Access-Control-Allow-Methods: GET'); }	
	public function methodPost(){ header('Access-Control-Allow-Methods: POST'); }	
	public function methodPut(){ header('Access-Control-Allow-Methods: PUT'); }	
	public function methodDelete(){ header('Access-Control-Allow-Methods: DELETE'); }	
	
} 