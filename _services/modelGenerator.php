<?php

function capitalizeName($string, $capitalizeFirstCharacter = false){

    $str = str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));

    if (!$capitalizeFirstCharacter) {
        $str[0] = strtolower($str[0]);
    }

    return $str;
}

require_once '../config.php';
require_once '../_services/Connection.php';

$id_conn =  new Connection();
$opCo = $id_conn->openConnection();

$table = trim(@$_REQUEST["table"]);

if ($table!=""){
    $sql = "SELECT 
    COLUMN_NAME
    FROM
    INFORMATION_SCHEMA.COLUMNS
    WHERE
    table_name = '$table'";

    $stmt = $opCo->prepare($sql);
    $stmt->execute();
    $fields = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
}

$insert = "";
$insert_params = "";
$update_params = "";

$b = "";


$b .= "&lt;?php";

$b .= ' 

require_once "../config.php";
require_once "../_services/Connection.php";

class '. capitalizeName($table,true) .' { 

    protected $con;
    protected $oCon;

    '; 

    foreach($fields as $l){
        $b .= ' private $'.$l.';
        ';
    }

    $b .= '

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    function __construct(){
        $this->con  = new Connection();
        $this->oCon = $this->con->openConnection();
    }

    function __destruct() {
        unset($this->oCon); unset($this->con);
    }
    ';

    $b .= '
    public function find($id){
        $stmt = $this->oCon->prepare(\'
        SELECT "'.capitalizeName($table,true).'", '.$table.'.* 
        FROM '.$table.' 
        WHERE id = :id
        \');
        $stmt->bindParam(\':id\', $id);
        $stmt->execute();
        
        return $stmt->fetchObject(__CLASS__);      
    }

    public function findAll(){
        $stmt = $this->oCon->prepare(\'SELECT * FROM '.$table.' \');
        $stmt->execute();
        return $stmt->fetchAll();  
    }
    ';

    $b .= '

    public function save(){
        if (!is_null($this->id)) { return $this->update();  }

        $stmt = $this->oCon->prepare(\'
        INSERT INTO  '.$table; 

        foreach($fields as $l){
            $insert .= $l.',';
            $insert_params .= ':'.$l.',';
        }

        $insert = substr_replace($insert, '', -1);
        $insert_params = substr_replace($insert_params, '', -1);

        $b.='
        ('.$insert.') 
        VALUES 
        ('.$insert_params .')\'); 

        ';


        foreach($fields as $l){
            $b .= ' $stmt->bindParam(\':'.$l.'\', $this->'.$l.');  
            ';
        }

        $b.= '
        return $stmt->execute();
    }

    public function update(){
        if (!isset($this->id) || (int)$this->id == 0 ) {
            throw new \LogicException(
            \'Cannot update user that does not yet exist in the database.\'
            );
        }

        $stmt = $this->oCon->prepare(\'
        UPDATE '.$table.' SET
            ';

        foreach($fields as $l){
            if($l!='id')
                $update_params .= '     '.$l.' = :'.$l.' ,  
            ';
        }

        $update_params = substr_replace($update_params, '', -17);


        $b.= $update_params;


        $b.='WHERE id = :id
        \');
        
        ';

        foreach($fields as $l){
            $b .= ' $stmt->bindParam(\':'.$l.'\', $this->'.$l.');  
            ';
        }

        $b.=' 
        return $stmt->execute();
    }
} ';


echo '<pre>'. $b. '<pre>';
