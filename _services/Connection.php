<?php

Class Connection {

	private    $host;
	private    $dbname;
 	private    $user;
	private    $pass;
	private    $options;  
	protected  $con;	 

	function __construct(){

		$this->host   = "localhost";
		$this->dbname = "gestao_opme";
   		$this->user = "root";
		$this->pass = "";
		$this->options  = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8' " );

		$this->openConnection();
 	} 

 	function __destruct(){ $this->closeConnection(); }

    public function openConnection(){

      	try{
       		$this->con = new PDO("mysql:host=".$this->host."; dbname=". $this->dbname , $this->user, $this->pass, $this->options);
   			return $this->con;
        }catch (PDOException $err){
            echo "There is some problem in connection: " . $err->getMessage();
        }    
    }
	
	public function closeConnection() { $this->con = null;	}

}

?>