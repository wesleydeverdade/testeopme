<?php

include_once '../../_services/AllowMethods.php';
$am = new AllowMethods();
$am->method = $_SERVER['REQUEST_METHOD'];

$ret = new StdClass();
$ret->sucesso = 1;
$ret->mensagem = "";

if( $am->method != 'GET' ){
	$ret->sucesso = 0;
	$ret->mensagem = "Você tem que passar o metódo GET para funcionar";
	die(json_encode($ret)); 
}

$am->start();

include_once '../../Controllers/ListarClientes.php';
$lc = new ListarClientes();

if(isset($_GET['nome'])) $lc->nome = $_GET['nome']; else $lc->nome = "";
if(isset($_GET['cnpj'])) $lc->cnpj = $_GET['cnpj']; else $lc->cnpj = "";

$lista = $lc->listaComFiltro();
die(json_encode($lista)); 