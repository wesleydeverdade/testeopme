<?php

include_once '../../_services/AllowMethods.php';
$am = new AllowMethods();
$am->method = $_SERVER['REQUEST_METHOD'];

$ret = new StdClass();
$ret->sucesso = 1;
$ret->mensagem = "";

if( $am->method != 'GET' ){
	$ret->sucesso = 0;
	$ret->mensagem = "Você tem que passar o metódo GET para funcionar";
	die(json_encode($ret)); 
}

$am->start();

include_once '../../Controllers/VisualizarCliente.php';
$vc = new VisualizarCliente();

if(isset($_GET['id_cliente'])) $vc->id_cliente = $_GET['id_cliente']; else $vc->id_cliente = "";
 
$lista = (array)$vc->listaPorId();
die(json_encode($lista)); 