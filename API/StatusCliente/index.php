<?php

include_once '../../_services/AllowMethods.php';
$am = new AllowMethods();
$am->method = $_SERVER['REQUEST_METHOD'];

$ret = new StdClass();
$ret->sucesso = 1;
$ret->mensagem = "";

if( $am->method != 'POST' ){
	$ret->sucesso = 0;
	$ret->mensagem = "Você tem que passar o metódo POST para funcionar";
	die(json_encode($ret)); 
}

$am->start();

include_once '../../Controllers/StatusCliente.php';
$sc = new StatusCliente();


if(isset($_GET['status'])) $sc->status = $_GET['status']; else $sc->status = "";
if(isset($_GET['id_cliente'])) $sc->id_cliente = $_GET['id_cliente']; else $sc->id_cliente = "";

if(((int)$sc->status==0 || (int)$sc->status==1) && $sc->id_cliente > 0 ){
	$lista = $sc->listaPorId();
	die(json_encode($lista)); 
}else{
	$ret->sucesso = 0;
	$ret->mensagem = "Você tem um cliente válido, ou um status válido para a inativação/ativação funcionar!";
	die(json_encode($ret)); 
}

